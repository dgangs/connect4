/*==============================================================
js/connect4.js
Connect4 module - connect4 FOTF coding exercise
	David Wygergangs (dwyger@live.com) submission

Connect4 classic game logic for multiple 2 player games.
================================================================*/

class Connect4 {
	/* classic connect 4 game for 2 players able to track over multiple games */

	constructor(startColor) {
		// initialize per game and per instance variables

		// per instance variables - persist for the life of the instance, not reset
		this.priorWinner = 'none'; // possible: none, yellow, or red.
		this.redWins = 0;
		this.yellowWins = 0;

		startColor = startColor.toLowerCase(); // set to lowercase for comparison
		if (startColor === 'red' || startColor === 'yellow') {
			this.firstColor = startColor;
		} else {
			// default to red if bad input provided.
			this.firstColor = 'red';
		}

		// per game variables - reset at the beginning of each game
		this.board = new Array(6);
		for (var i = 0;i < 6; i++) {
			this.board[i] = new Array(7).fill('O'); // fill each board column with O's for nothing / empty
		}
		this.gameState = 'inplay'; // possible: inplay or finished
		this.gameWinner = 'none'; //none until clear current winner
		this.turnColor = this.firstColor; //player turn color
		this.winCheckers = []; //winning checkers series of objects {col: #, row: #}

		return this.firstColor + ' has the first turn'; // returns first player turn
	}

	newGame() {
		// reset board, game state, and variables for a new game
		this.priorWinner = this.gameWinner // assign last winner to prior
		this.board = new Array(6);
		for (var i = 0;i < 6; i++) {
			this.board[i] = new Array(7).fill('O'); // fill each board column with O's for nothing / empty
		}
		this.gameState = 'inplay'; // reset to inplay
		this.gameWinner = 'none'; //none until clear current winner
		this.winCheckers = []; // reset list of winning checkers
		// determine player turn
		if (this.priorWinner === 'red' || this.priorWinner === 'yellow') {
			// if prior game was not a draw, the loser goes first, otherwise leave player turn from last game
			this.turnColor = (this.priorWinner === 'red') ? 'yellow' : 'red';
		}

		return this.turnColor; //player to go first in this game.
	}

	dropChecker(column) {
		// Method to crop checkers onto the board
		var lightUp = {
					slot: '',
					color: this.turnColor
				} // checker to light up on the HTML board
		var dropSlot = {col: 0, row: 0} //drop checker slot
			//first check a game is in progress
		if (this.gameState !== 'inplay') throw new Error('no game is in progress - start a new one');
			//check column is inbounds 0-5
		if (column < 0 || column > 5) throw new Error('column is out of bounds for checker drop');
			//check column isn't already full of checkers
		if (this.board[column][0] !== 'O') {
			//this column is full
			throw new Error('column is already full');

		}
			//now descend the column to find the top of the column to place the checker.
		for(var i = 1; i < 7; i++) {
			// look for first not O element
			if (this.board[column][i] !== 'O') {
				// checker above it using uppercase R or Y for player color.
				this.board[column][i - 1] = this.turnColor[0].toUpperCase();
				lightUp.slot = 'slot' + (column + 1) + '-' + i;
				dropSlot = {col: column, row: i - 1};
				break;
			} else if (i === 6) {
				// empty column
				this.board[column][i] = this.turnColor[0].toUpperCase();
				lightUp.slot = 'slot' + (column + 1) + '-7';
				dropSlot = {col: column, row: i};
			}
		}

		// check for any wins for current player
		this.checkForWin(dropSlot); // Check if a player wins with this drop.

		// switch turns
		if (this.turnColor === 'red') {
			// red just went - now yellow's turn
			this.turnColor = 'yellow';
		} else {
			// yellow just went - now red's turn
			this.turnColor = 'red';
		}
		return lightUp;
	}

	get nextPlay() {
		// Identifies game progression, whether next players turn, who goes - or if someone wins, who won.
			/* Return:
				'red turn / yellow Turn / red wins / yellow wins / draw' */
			switch (this.gameState) {
				case 'inplay':
					return this.turnColor + ' turn';
				case 'finished':
					return this.gameWinner + 'wins';
					break;
			}
	}

	get whosTurn() {
		return this.turnColor;
	}

	get winSeries() {
		// retrieve the checkers of the winning series to highlight.
		return this.winCheckers;
	}

	get winner() {
		// retrieve the game winner.
		return this.gameWinner;
	}

	checkForWin(slot) {
		// check for 4 checker row from provided slot
			//slot parameter: slot = { col: 0-5, row: 0-6}
			//if win: change this.gameState
		var check4 = []; // collect winning checkers as {col:0,row:0} objects in array
		var checker = this.board[slot.col][slot.row]; //get the exact color checker to match
		var fourConnected = false; // flag for win found
		//Traverse vertical (only have down to traverse since checker lays on top)
		if (slot.row < 4) {
			// stacked high enough for a possible vertical series
			var checkers = new Array({col: slot.col, row: slot.row}); //starting slot checker
			for(var i = slot.row + 1; i < 7; i++) {
				// step through adjacent for matches
				if (this.board[slot.col][i] === checker) {
					// save adjacent checker location
					checkers.push({col: slot.col, row: i})
				} else {
					// hit different checker
					break;
				}
			}
			if (checkers.length >= 4) {
				// player wins with 4 checkers connected
				fourConnected = true;
				check4 = check4.concat(checkers); // copy connected checkers for highligting results
			}
		}

		//Traverse horizontal
		if (!fourConnected) {
			var checkers = new Array({col: slot.col, row: slot.row}); //starting slot checker
			if (slot.col > 0) {
				// at least one column to the left to walk
				for(var i = slot.col - 1; i >= 0; i--) {
					// step left through adjacent matches
					if (this.board[i][slot.row] === checker) {
						// save adjacent checker location
						checkers.push({col: i, row: slot.row})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (slot.col < 5) {
				// at least one column to the right to walk
				for(var i = slot.col + 1; i < 6; i++) {
					// step right through adjacent matches
					if (this.board[i][slot.row] === checker) {
						// save adjacent checker location
						checkers.push({col: i, row: slot.row})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (checkers.length >= 4) {
				// player wins with 4 checkers connected
				fourConnected = true;
				if (check4.length > 0) {
					// matched already happend on another axis, remove center checker to prevent duplicate
					checkers.shift(); //remove the first element, the center checker
				}
				check4 = check4.concat(checkers); // copy connected checkers for highlighting results
			}
		}
		//Traverse Top left - down diagonal
		if (!fourConnected) {
			var checkers = new Array({col: slot.col, row: slot.row}); //starting slot checker
			if (slot.col > 0 && slot.row > 0) {
				// walk up to the left for matches past column 0 and row 0.
				var maxDistance = Math.min(slot.col , slot.row); //distance until an edge reached
				for(var i = 1; i <= maxDistance; i++) {
					// step left and up through adjacent matches
					if (this.board[slot.col - i][slot.row - i] === checker) {
						// save adjacent checker location
						checkers.push({col: slot.col - i, row: slot.row - i})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (slot.col < 5 && slot.row < 6) {
				// walk down to the right for matches above past column 5 and row 6.
				var maxDistance = Math.min(5 - slot.col , 6 - slot.row); //distance until an edge reached
				for(var i = 1; i <= maxDistance; i++) {
					// step right and down through adjacent matches
					if (this.board[slot.col + i][slot.row + i] === checker) {
						// save adjacent checker location
						checkers.push({col: slot.col + i, row: slot.row + i})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (checkers.length >= 4) {
				// player wins with 4 checkers connected
				fourConnected = true;
				if (check4.length > 0) {
					// matched already happend on another axis, remove center checker to prevent duplicate
					checkers.shift(); //remove the first element, the center checker
				}
				check4 = check4.concat(checkers); // copy connected checkers for highlighting results
			}
		}

		//Traverse Top right - down diagonal
		if (!fourConnected) {
			var checkers = new Array({col: slot.col, row: slot.row}); //starting slot checker
			if (slot.col < 5 && slot.row > 0) {
				// walk up to the right for matches past column 5 and row 0.
				var maxDistance = Math.min(5 - slot.col , slot.row); //distance until an edge reached
				for(var i = 1; i <= maxDistance; i++) {
					// step right and up through adjacent matches
					if (this.board[slot.col + i][slot.row - i] === checker) {
						// save adjacent checker location
						checkers.push({col: slot.col + i, row: slot.row - i})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (slot.col > 0 && slot.row < 6) {
				// walk down to the left for matches past column 0 and row 6.
				var maxDistance = Math.min(slot.col , 6 - slot.row); //distance until an edge reached
				for(var i = 1; i <= maxDistance; i++) {
					// step down and left through adjacent matches
					if (this.board[slot.col - i][slot.row + i] === checker) {
						// save adjacent checker location
						checkers.push({col: slot.col - i, row: slot.row + i})
					} else {
						// hit different checker
						break;
					}
				}
			}
			if (checkers.length >= 4) {
				// player wins with 4 checkers connected
				fourConnected = true;
				if (check4.length > 0) {
					// matched already happend on another axis, remove center checker to prevent duplicate
					checkers.shift(); //remove the first element, the center checker
				}
				check4 = check4.concat(checkers); // copy connected checkers for highlighting results
			}
		}
		//Set game results
		if (fourConnected) {
			this.winCheckers = this.winCheckers.concat(check4);
			this.gameState = 'finished';
			this.gameWinner = (checker === 'R') ? 'red' : 'yellow';
			if(this.gameWinner === 'red') {
				this.redWins++;
			} else {
				this.yellowWins++;
			}
		} else if (this.boardFull()) {
			//check for possible draw game
			this.gameState = 'finished';
			this.gameWinner = 'none';
		}
	}

	boardFull() {
		//check if all columns are filled by looking across the top row
		var tally = 0;
		for (var i = 0; i < 6; i++) {
			if (this.board[i][0] !== 'O') {
				//filled column
				tally++;
			}
		}
		if (tally === 6 && this.gameState === 'inplay') {
			//all columns are filled draw game since not already won
			this.gameState = 'finished';
			this.gameWinner = 'none';
			return true;
		}
		return false;
	}

}