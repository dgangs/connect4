/*=====================================================================
js/index.js
Connect 4 main initialization - FOTF coding exercise
		David Wygergangs (dwyger@live.com) submission

Initialize the game.
Manage events and user interactions.
======================================================================*/

var gameCycle = 'choose'; // tracks cycle for input buttons can be 'choose' OR 'play-again' (whether 1st game or repeat)
var connect4Games; // connect4 game object
var messageBoard; // Used for diplaying messages back to the players
var redScore; // red player score box
var yellowScore; // yellow player score box
var buttons; // user input buttons
var columns; // board columns
var winOutline = 5; // winning outline size to be updated by resize function (sizeBoard)



window.onload = function() {
	// Wait for window load before proceeding with these initialization components.

	sizeBoard(); //size board appropriately to the screen
	window.addEventListener('resize' , debounce(sizeBoard, 100));

	messageBoard = document.querySelector('#message'); 
	redScore = document.querySelector('#r-score');
	yellowScore = document.querySelector('#y-score');
	buttons = Array.from(document.querySelectorAll('#messages>button'));
	columns = Array.from(document.querySelectorAll('.column'));

	// Buttons first appear to ask to start a game so handler kicks off game flow...
	buttons.forEach(btn => {
		btn.addEventListener('click' , messageButtons);
	});

}

function messageButtons(event) {
	// handle responses to player questions about checker colors, new games and transition game states
	switch (gameCycle) {
		case 'choose':
			// Ask player what color to start with, capture from button text
			connect4Games = new Connect4(event.target.innerHTML.toLowerCase());
			// hide buttons
			toggleButtons();
			messageBoard.innerHTML = "You are the <span id='" + connect4Games.whosTurn + "'>" + connect4Games.whosTurn + "</span> player! Pick a column to get started!";

			// enable listener event on the columns for players dropping checkers
			columns.forEach(col => {
				col.addEventListener('click' , clickHandler);
			});
			gameCycle = 'play-again'; // now on button questions will just be for replay
		break;
		case 'play-again':
			// Check if player does want to play again.
			if (event.target.innerHTML.toLowerCase() === 'yes') {
				// player wants to play again
				boardCleanUp(); // remove the board checkers and highlights
				var firstPlayer = connect4Games.newGame();
				toggleButtons();
				messageBoard.innerHTML = "New game started! <span id='" + firstPlayer + "'>" + firstPlayer[0].toUpperCase() + firstPlayer.slice(1) + "</span> player go first, pick a column.";
			} else {
				// players are done playing - thank them for playing
				toggleButtons(); // clear buttons
				messageBoard.innerHTML = "Thank you for playing CONNECT 4!";
				// remove column click listener for dropping checkers
				columns.forEach(col => {
					col.removeEventListener('click' , clickHandler);
				});
			}
		break;
	};
}

function clickHandler(event) {
	// Attach to board columns for player checker drops
	var dropColumn = event.target.getAttribute('data-col'); // direct access of dataset fails in Edge
	if (dropColumn) {
		// if event has the property call method to drop checker
		try {
			var checker = connect4Games.dropChecker(dropColumn - 1);
			placeChecker(checker);
			if (connect4Games.nextPlay.includes('turn')){
				// game is in play, and a players turn
				messageBoard.innerHTML = "It's the <span id='" + connect4Games.whosTurn + "'>" + connect4Games.whosTurn + "</span> player's turn.  Place your checker.";
			} else if (connect4Games.nextPlay.includes('wins')) {
				// the game is over (check for a win or draw)
				if (connect4Games.winner !== 'none') {
					//not a draw game
					messageBoard.innerHTML = "<span id='" + connect4Games.winner + "'>" + connect4Games.gameWinner.toUpperCase() + "</span> player wins! Want to play again?";
					toggleButtons(['Yes','No']);
					winLine(connect4Games.winSeries);
					setScores(); // update score board
				} else {
					messageBoard.innerHTML = 'Tough competition - game was a draw! Want to play again?';
					toggleButtons(['Yes','No']);
				}
			}
		} catch (exception) {
			if (exception.message.includes('column is already full')) {
				messageBoard.innerHTML = "<span id='" + connect4Games.whosTurn + "'>" + connect4Games.whosTurn[0].toUpperCase() + connect4Games.whosTurn.slice(1) + "</span>-player that column is full, please pick a different one.";
			} else if (exception.message.includes('no game is in progress')) {
				messageBoard.innerHTML = 'Do you want to play CONNECT 4 again?';
			} else {
				throw exception;
			}

		}

	}

}

function placeChecker(slot) {
	// function light up the slots for each checker
		// slot = {slot: 'element-id', color: 'red/yellow'}

		// get slot element
	var slotCircle = document.querySelector('#' + slot.slot);

		// add class to color for the appropriate player
	if (slot.color === 'red') {
		slotCircle.classList.add('red-checker');
	} else if (slot.color === 'yellow') {
		slotCircle.classList.add('yellow-checker');
	}
}

function winLine(checkers) {
	// highlight winning checkers line
		//input: checkers array of {col: 0, row: 0}
	var winStyle = `stroke: limegreen; stroke-width: ${winOutline}px`
	checkers.forEach(checker => {
		document.querySelector(`#slot${checker.col + 1}-${checker.row + 1}`).setAttribute('style', winStyle);
	});
}

function toggleButtons(prompts) {
	// update user button prompts

	// hide/unhide buttons
	buttons.forEach(btn => {
		btn.classList.toggle('hide');
	});
	if (prompts) {
		// new prompts provided update
		buttons[0].innerHTML = prompts[0];
		buttons[1].innerHTML = prompts[1];
	}
}

function setScores() {
	// update the scoreboard
	redScore.value = connect4Games.redWins;
	yellowScore.value = connect4Games.yellowWins;
}

function boardCleanUp() {
	// clear the board of checkers and highlights CSS
	Array.from(document.querySelectorAll('circle')).forEach(circle => {
		circle.classList.remove('red-checker');
		circle.classList.remove('yellow-checker');
		circle.removeAttribute('style'); // from winning checkers
	})
}

function sizeBoard() {
	// set size of the board for variable screen sizes

	var windowH = window.innerHeight; // window height in px
	var windowW = window.innerWidth; // window width in px
	var heightRatio = 0.65; // ratio of board height to window height
	var widthRatio = 0.90; // ratio of window width to use if up against width viewport constraint
	var boardH = windowH * heightRatio; // board height in px
	var columnW = boardH / 7; // column width in px

	// verify width fits screen also
	if ((columnW * 7) > windowW) {
		//approximately larger than available window view
		columnW = windowW * widthRatio / 6; // recalculate width in px
		boardH = columnW * 7; // recalculate board height in px
	}
	var cx = columnW / 2; // circle x coordinate in px
	var radiusRatio = 0.87;	// ratio of radius to grid square area
	var radius = cx * radiusRatio ; // checker radius in px
	var padding = Math.abs(windowW / 100); // relative horizontal padding space
	winOutline = cx * 0.15; // update relative winning checkers outline size

	// complete calculations of cy columns
	// update circle dimensions
	for (var column = 0; column < 6; column++) {
		// walk the columns to update column dimensions
		var stack = document.querySelector(`#stack-${column + 1}`); // get column
		stack.setAttribute('height' , boardH + 'px');
		stack.setAttribute('width' , columnW + 'px');
		stack.setAttribute('padding' , '0 ' + padding + 'px');
		stack.classList.remove('stacks'); //remove defaults
		for (var row = 0; row < 7; row++) {
			//walk the rows to update the circle dimensions
			var circle = document.querySelector(`#slot${column + 1}-${row + 1}`);
			circle.setAttribute('r' , radius + 'px');
			circle.setAttribute('cx' , cx + 'px');
			circle.setAttribute('cy' , (cx + (columnW * row)) + 'px');
		}
	}

}

function debounce(work, delay, immediate) {
	// helper function to prevent over-use of taxing functions
	var timeout;
	return function() {
		var context = this;
		var args = arguments;
		var next = function() { //Future function to call the work function after a delay
			timeout = null;
			if (!immediate) work.apply(context, args);
		}
		var triggerNow = immediate && !timeout;
		clearTimeout(timeout); // Stops any work functions queued until events stop firing for delay duration
		timeout = setTimeout(next, delay);
		if (triggerNow) work.apply(context, args); // Last in the queue fires or if 'immediate' set
	}
}
